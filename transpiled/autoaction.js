'use strict';

// import storeShape from 'react-redux/lib/utils/storeShape';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

exports.default = autoaction;

var _shallowEqual = require('react-redux/lib/utils/shallowEqual');

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _deepEqual = require('deep-equal');

var _deepEqual2 = _interopRequireDefault(_deepEqual);

var _redux = require('redux');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var BatchActions = {
  isDispatching: false,

  // Needs:
  //   - action name
  //   - arguments
  //   - function wrapped with dispatcher

  // object in the format of:
  //   {
  //     actionName: [
  //       {func, args, key},
  //       {func, args, key},
  //     ],
  //   }
  queue: {},

  called: {},

  // tryDispatch iterates through all queued actions and dispatches actions
  // with unique arguments.
  //
  // Each dispatch changes store state; our wrapper component listens to store
  // changes and queues/dispatches more actions.  This means that we need to
  // remove actions from our queue just before they're dispatched to prevent
  // stack overflows.
  tryDispatch: function tryDispatch() {
    var _this = this;

    Object.keys(this.queue).forEach(function (actionName) {
      var calls = _this.queue[actionName] || [];

      // Iterate through all of this action's batched calls and dedupe
      // if arguments are the same
      calls = calls.reduce(function (uniq, call, idx) {
        // if the args and key arent the same this is a new unique call
        var isUnique = uniq.every(function (prev) {
          // Only test keys if the current call has a key and it doesn't match
          // the previous key.
          var isKeyMatch = call.key !== null && prev.key === call.key;
          var isArgMatch = (0, _deepEqual2.default)(prev.args, call.args);

          // If both the action args and keys match this is non-unique, so
          // return false.
          return !(isKeyMatch === true && isArgMatch === true);
        });

        if (isUnique) {
          uniq.push(call);
        }

        return uniq;
      }, []);

      delete _this.queue[actionName];

      // call each deduped action and pass in the required args
      calls.forEach(function (call, idx) {
        if (Array.isArray(call.args)) {
          return call.func.apply(null, call.args);
        }
        // this is either an object or single argument; call as expected
        return call.func(call.args);
      });
    });

    this.queue = {};
  },
  enqueue: function enqueue(actionName, func, args, key) {
    var actions = this.queue[actionName] || [];
    actions.push({
      args: args,
      func: func,
      key: key
    });
    this.queue[actionName] = actions;
  }
};

/**
 * autoaction is an ES7 decorator which wraps a component with declarative
 * action calls.
 *
 * The actions map must be in the format of { actionName: stateFunc }
 *
 * Example usage:
 *
 *   @autoaction({
 *     getBlogPost: (state) => { return { org: state.params.router.slug }; }
 *   })
 *   @connect(mapState)
 *   class Post extends React.Component {
 *
 *     static propTypes = {
 *       post: React.PropTypes.object.isRequired
 *     }
 *
 *     render() {
 *       ...
 *     }
 *
 *   }
 *
 */
function autoaction() {
  var autoActions = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var actionCreators = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  // Overall goal:
  // 1. connect to the redux store
  // 2. subscribe to data changes
  // 3. compute arguments for each action call
  // 4. if any arguments are different call that action bound to the dispatcher

  // We refer to this many times throughout this function
  var actionNames = Object.keys(autoActions);

  // If we're calling actions which have no functions to prepare arguments we
  // don't need to subscribe to store changes, as there is nothing from the
  // store that we need to process.
  var shouldSubscribe = actionNames.length > 0 && actionNames.some(function (k) {
    return typeof autoActions[k] === 'function';
  });

  // Given a redux store and a list of actions to state maps, compute all
  // arguments for each action using autoActions passed into the decorator and
  // return a map contianing the action args and any keys for invalidation.
  function computeAllActions(props, state) {
    return actionNames.reduce(function (computed, action) {
      var data = autoActions[action];

      if (Array.isArray(data)) {
        computed[action] = {
          args: data,
          key: null
        };
        return computed;
      }

      // we may have an arg function or an object containing arg and key
      // functions.
      switch (typeof data === 'undefined' ? 'undefined' : _typeof(data)) {
        case 'function':
          computed[action] = {
            args: data(props, state),
            key: null
          };
          break;
        case 'object':
          var args = data.args;
          // If we're passed a function which calcs args based on props/state,
          // call it. Otherwise assume that data.args is a single type to be
          // used as the argument itsekf
          if (typeof data === 'function') {
            args = args(props, state);
          }
          computed[action] = {
            args: args,
            key: data.key(props, state)
          };
          break;
        default:
          // By default use this as the argument
          computed[action] = {
            args: data,
            key: null
          };
      }
      return computed;
    }, {});
  }

  // If any argument within any action call is undefined our arguments should be
  // considered invalid and this should return true.
  function areActionArgsInvalid(args) {
    // single argument actions
    if (args === undefined) {
      return true;
    }
    if (Array.isArray(args)) {
      return args.some(function (arg) {
        return arg === undefined;
      });
    }
    if ((typeof args === 'undefined' ? 'undefined' : _typeof(args)) === 'object') {
      return Object.keys(args).some(function (arg) {
        return args[arg] === undefined;
      });
    }
    // TODO: throw an invariant here
    return false;
  }

  return function (WrappedComponent) {
    var _class, _temp;

    /**
     * Autoconect is a wrapper component that:
     *   1. Resolves action arguments via selectors from global state
     *   2. Automatically calls redux actions with the determined args
     *
     * This lets us declaratively call actions from any component, which in short:
     *   1. Allows us to declaratively load data
     *   2. Reduces boilerplate for loading data across componentWillMount and
     *      componentWillReceiveProps
     */
    return _temp = _class = function (_React$Component) {
      _inherits(autoaction, _React$Component);

      function autoaction(props, context) {
        _classCallCheck(this, autoaction);

        var _this2 = _possibleConstructorReturn(this, (autoaction.__proto__ || Object.getPrototypeOf(autoaction)).call(this, props, context));

        _this2.store = context.store;
        _this2.mappedActions = computeAllActions(props, _this2.store.getState());
        _this2.actionCreators = (0, _redux.bindActionCreators)(actionCreators, _this2.store.dispatch);
        return _this2;
      }

      _createClass(autoaction, [{
        key: 'componentWillMount',
        value: function componentWillMount() {
          this.tryCreators();
        }
      }, {
        key: 'componentDidMount',
        value: function componentDidMount() {
          this.trySubscribe();
          BatchActions.tryDispatch();
        }
      }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
          this.tryUnsubscribe();
        }
      }, {
        key: 'trySubscribe',
        value: function trySubscribe() {
          if (shouldSubscribe && !this.unsubscribe) {
            this.unsubscribe = this.store.subscribe(this.handleStoreChange.bind(this));
          }
        }
      }, {
        key: 'tryUnsubscribe',
        value: function tryUnsubscribe() {
          if (typeof this.unsubscribe === 'function') {
            this.unsubscribe();
            this.unsubscribe = null;
          }
        }

        // When the Redux store recevies new state this is called immediately (via
        // trySubscribe).
        //
        // Each action called via autoaction can use props to determine arguments
        // for the action.
        //
        // Unfortunately, we're listening to the store directly.  This means that
        // `handleStoreChange` may be called before any parent components receive
        // new props and pass new props to our autoaction component.  That is
        // - the parent component hasn't yet received the store update event and
        // the passed props are out-of-sync with actual store state (they're
        // stale).
        //
        // By computing actions within a requestAnimationFrame window we can
        // guarantee that components have been repainted and any parent @connect
        // calls have received new props.  This means that our props used as
        // arguments within autoconnect will always be in sync with store state.
        //
        // This is kinda complex.  Trust us, this works.
        //
        // TODO: Write test case.

      }, {
        key: 'handleStoreChange',
        value: function handleStoreChange() {
          var _this3 = this;

          var handleChange = function handleChange() {
            var actions = computeAllActions(_this3.props, _this3.store.getState());
            if ((0, _deepEqual2.default)(actions, _this3.mappedActions)) {
              return;
            }

            _this3.tryCreators(actions);
            BatchActions.tryDispatch();
          };

          // See above comments for explanation on using RAF.
          if (window && window.requestAnimationFrame) {
            window.requestAnimationFrame(handleChange);
          } else {
            handleChange();
          }
        }

        // Iterate through all actions with their computed arguments and call them
        // if necessary.
        // We only call the action if all arguments !== undefined and:
        //   - this is the first time calling tryCreators, or
        //   - the arguments to the action have changed

      }, {
        key: 'tryCreators',
        value: function tryCreators() {
          var _this4 = this;

          var actions = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.mappedActions;

          // If we're calling tryCreators with this.mappedActions we've never
          // called the actions before.
          var initialActions = actions === this.mappedActions;

          Object.keys(actions).forEach(function (a) {
            var action = actions[a];

            if (areActionArgsInvalid(action.args)) {
              // TODO: LOG
              return;
            }

            if (initialActions || !(0, _deepEqual2.default)(action, _this4.mappedActions[a])) {
              _this4.mappedActions[a] = action;
              BatchActions.enqueue(a, _this4.actionCreators[a], action.args, action.key);
            }
          });
        }
      }, {
        key: 'shouldComponentUpdate',
        value: function shouldComponentUpdate(nextProps, nextState) {
          return !(0, _shallowEqual2.default)(nextProps, this.props);
        }
      }, {
        key: 'render',
        value: function render() {
          return _react2.default.createElement(WrappedComponent, this.props);
        }
      }]);

      return autoaction;
    }(_react2.default.Component), _class.contextTypes = {
      store: _propTypes2.default.any
    }, _temp;
  };
}